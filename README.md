# Tüccarefendi API PHP Client Projesi #

Tüccarefendi Sistemine ait API alt yapısının PHP dili ile kullanılması tavsiye edilen yapıdır. Umumi manada API' nin nasıl kullanılacağına dair bilgiler bulundurmakla birlikte, API' nin PHP ile nasıl kullanılacağına dair kod ve kütüphaneyi içermektedir.

Projenin orjinali [thepleague/oauth2](https://github.com/thephpleague/oauth2-client) isimli projedir. Bu projenin üzerinde düzenlemeler gerçekleştirilmiştir.

Kaynak kodumuza [buradan](https://bitbucket.org/tuccarefendi-api/tuccarefendi-api-php-client/src/bb9aee353f3dc38788a1931a2194ca70a2f8d3a8?at=master) ulaşabilirsiniz.

Wikimize [şuradan](https://bitbucket.org/tuccarefendi-api/tuccarefendi-api-php-client/wiki/Home) ulaşabilirsiniz.

Sıkıntı ve isteklerinizi [oraya](https://bitbucket.org/tuccarefendi-api/tuccarefendi-api-php-client/issues?status=new&status=open) bildirebilirsiniz.