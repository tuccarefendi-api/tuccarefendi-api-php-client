<?php

include("configs.php");
require_once __DIR__.'/vendor/autoload.php';

$provider = new \League\OAuth2\Client\Provider\GenericProvider([
	'clientId'                => $ClientId,       // The client ID assigned to you by the provider
	'clientSecret'            => $ClientSecret,   // The client password assigned to you by the provider
	'redirectUri'             => $RedirectUri,    //Your redirectUri Bu bilgileri configs.php dosyasından değiştirebilirsiniz.
	'urlAuthorize'            => 'http://api.tuccarefendi.com/authorize.php',
	'urlAccessToken'          => 'http://api.tuccarefendi.com/token.php',
	'urlResourceOwnerDetails' => 'http://api.tuccarefendi.com/resource.php',
]);

// $request=$provider->getAuthenticatedRequest("GET", "http://api.tuccarefendi.com/v1/users/get/", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("GET", "http://api.tuccarefendi.com/v1/subStores/list/", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("GET", "http://api.tuccarefendi.com/v1/orders/list/?costumerName=atih", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("GET", "http://api.tuccarefendi.com/v1/orders/get/2", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("POST", "http://api.tuccarefendi.com/v1/orders/accept/", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// $request=$request->withHeader('Content-Type', 'application/x-www-form-urlencoded');
// $request->getBody()->write(http_build_query(array("orderItemId"=>2)));
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("POST", "http://api.tuccarefendi.com/v1/orders/reject/", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// $request=$request->withHeader('Content-Type', 'application/x-www-form-urlencoded');
// $request->getBody()->write(http_build_query(array("orderItemId"=>2, "rejectReasonType"=>1, "rejectReasonText"=>"Stokta kalmadı.")));
// print_r($provider->getResponse($request));

// $request=$provider->getAuthenticatedRequest("POST", "http://api.tuccarefendi.com/v1/orders/sendCargo/", "2a662d337a8eabcea36d7c361c570c1f42bf9bf3");
// $request=$request->withHeader('Content-Type', 'application/x-www-form-urlencoded');
// $request->getBody()->write(http_build_query(array("orderItemId"=>2, "trackCode"=>"TAKIPKODU123", "branch"=>"Maslak")));
// print_r($provider->getResponse($request));

?>