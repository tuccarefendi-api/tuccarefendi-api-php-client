<?php

include("configs.php");
require_once __DIR__.'/vendor/autoload.php';
session_start();
$provider = new \League\OAuth2\Client\Provider\GenericProvider([
	'clientId'                => $ClientId,       // The client ID assigned to you by the provider
	'clientSecret'            => $ClientSecret,   // The client password assigned to you by the provider
	'redirectUri'             => $RedirectUri,    //Your redirectUri Bu bilgileri configs.php dosyasından değiştirebilirsiniz.
	'urlAuthorize'            => 'http://api.tuccarefendi.com/authorize.php',
	'urlAccessToken'          => 'http://api.tuccarefendi.com/token.php',
	'urlResourceOwnerDetails' => 'http://api.tuccarefendi.com/resource.php',
]);
// If we don't have an authorization code then get one
if (!isset($_GET['code'])) {
	// Fetch the authorization URL from the provider; this returns the
	// urlAuthorize option and generates and applies any necessary parameters
	// (e.g. state).
	$authorizationUrl = $provider->getAuthorizationUrl();

	// Get the state generated for you and store it to the session.
	$_SESSION['oauth2state'] = $provider->getState();

	// Redirect the user to the authorization URL.
	header('Location: ' . $authorizationUrl."&mailaddress=".$_GET["mailaddress"]);
	exit;

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
	unset($_SESSION['oauth2state']);
	exit('Invalid state');

} else {

	try {

		// Try to get an access token using the authorization code grant.
		$accessToken = $provider->getAccessToken('authorization_code', [
			'code' => $_GET['code']
		]);

		// We have an access token, which we may use in authenticated
		// requests against the service provider's API.

		// ACCESSTOKEN BİLGİLERİNİZİ VERİTABANINIZA KAYDEDEBİLİRSİNİZ.
		echo $accessToken->getToken()."<br/>";
		echo $accessToken->getRefreshToken()."<br/>";
		#$accessToken->getExpires();
		#$accessToken->hasExpired() ? 'expired' : 'not expired';


	} catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

		// Failed to get the access token or user details.
		exit($e->getMessage());

	}

}
?>