<?php

include("configs.php");
require_once __DIR__.'/vendor/autoload.php';

$provider = new \League\OAuth2\Client\Provider\GenericProvider([
	'clientId'                => $ClientId,       // The client ID assigned to you by the provider
	'clientSecret'            => $ClientSecret,   // The client password assigned to you by the provider
	'redirectUri'             => $RedirectUri,    //Your redirectUri Bu bilgileri configs.php dosyasından değiştirebilirsiniz.
	'urlAuthorize'            => 'http://api.tuccarefendi.com/authorize.php',
	'urlAccessToken'          => 'http://api.tuccarefendi.com/token.php',
	'urlResourceOwnerDetails' => 'http://api.tuccarefendi.com/resource.php',
]);

// Kodunuzu buraya yazabilirsiniz.

?>