 <?php
 include("configs.php");
require_once __DIR__.'/vendor/autoload.php';
$provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => $ClientId,    
    'clientSecret'            => $ClientSecret,  
    'redirectUri'             => $RedirectUri,
     'urlAuthorize'            => 'http://api.tuccarefendi.com/authorize.php',
    'urlAccessToken'          => 'http://api.tuccarefendi.com/token.php',
    'urlResourceOwnerDetails' => 'http://api.tuccarefendi.com/resource.php',
	
]);
   //Access Token'ı authorization işleminden sonra aldığınınz refresh tokenla bu sayfa üzerinden değiştirebilirsiniz.
   //Bu işlem ile yeni access token ve refresh token'ı elde etmiş oluyoruz.
   //Aldığınız yeni access token ve refresh token'ı veritabanınıza kaydedebilirsiniz.
 
       // Yeni Access Token;
   $RefreshToken = $_GET['refresh_token'];

    $newAccessToken = $provider->getAccessToken('refresh_token', [
        'refresh_token' => $RefreshToken]);
		
		 //Yeni Refresh Token;
	
	echo $newAccessToken->getRefreshToken();  
    
	// Purge old access token and store new access token to your data store.


?>